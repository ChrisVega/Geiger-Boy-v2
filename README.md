# Geiger-Boy v.2
Half GameBoy, half Geiger counter. It's the Geiger Boy!

## About
[Original Geiger Boy project.](https://gitlab.com/ChrisVega/Geiger-Boy)

[Requires SoftI2CMaster.](https://github.com/felias-fogg/SoftI2CMaster)

Version 2 features a few new features and improvements:

- Added FRAM to store total accumulated dosage
- Used "time slices" to achieve faster measurements while not sacrificing accuracy
- Used a smaller Geiger counter circuit allowing for internal battery storage
- Green housing

## Design procedure and process
Working off of the previous project the first step was to integrate the FRAM with the current code. Initially this was done by taking the library made by Adafruit, as well as the OLED library, and modifying them to use TinyWireM. Once this worked the circuits for the reset button and pulse extender with a buzzer were added as well and all tested. While this working the screen was taking too long to refresh, to solve this SoftI2CMaster was used instead of TinyWireM. The libraries for the OLED display and FRAM were combined into one as SoftI2CMaster can only be initialized once. The circuit was then assembled and placed in the housing further testing was done to ensure it would work properly after being on for a long amount of time.

## Parts, procedure and specifications
To accomplish these improvements a smaller Geiger counter circuit, also provided by [RH electronics](http://www.rhelectronics.net/store/high-voltage-geiger-probe-driver-power-supply-module-420v-550v-with-ttl-digitized-pulse-output.html) will be utilized. For storing and displaying the total accumulated dose and FRAM chip will be utilized due to its large amount read write cycles. A high write cycle is needed as the total accumulated dose must be read, updated, and written onto the FRAM every 5 seconds when a reading is completed. [Adafruit supplies a break out board](https://www.adafruit.com/product/1895) for an FRAM chip allowing for easier connections.

There are two switches, the power switch on the top and a safety switch on the side and one button to reset total accumulated dosage. Safety switch prevents accidentally resetting of your total accumulated dosage, when off the button will not do anything. 

Measurements are taken in five second intervals using “time slices”. How this works is you have an array of size two. Measurements will switch between the two positions in the array which would be your two time slices. When a measurement is complete it is stored in the current position and switched to the next. This means that you can use the previous measurement, in combination with the new, to estimate a 10 sec reading that can be updated every 5 sec. 

### Parts 
- GameBoy housing
- RH electronics Geiger counter circuit
- SBM-20 Geiger tube
- Ssd1306 OLED
- ATtiny85
- Adafruit FRAM breakout board
- Perf board for ATtiny shield, button and buzzer circuits
- Switches
- Push button
- Various resistors and capacitors
- Passive buzzer
- Wires
- LED

## Progress
As of now total accumulated dosage is working as expected and a prototype has been constructed. New time slice code is still being worked on and tested and neater circuitry must be designed.
