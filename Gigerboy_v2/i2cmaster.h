/*
   SSD1306xLED and Adafrait FRAM libraries mereged to use softi2cmaster
*/

#include <stdint.h>
#include <Arduino.h>

#ifndef i2cmaster_H
#define i2cmaster_H

// ----------------------------------------------------------------------------

#ifndef SSD1306_SA
#define SSD1306_SA		0x3C	// Slave address
#endif

#define MB85RC_DEFAULT_ADDRESS        (0x50) /* 1010 + A2 + A1 + A0 = 0x50 default */
#define MB85RC_SLAVE_ID       (0xF8)

// ----------------------------------------------------------------------------

class i2cmaster
{
  public:
    i2cmaster(void);
    void  begin(uint8_t addr = MB85RC_DEFAULT_ADDRESS);
    void ssd1306_send_byte(uint8_t type, uint8_t byte);
    void ssd1306_send_command(uint8_t command);
    void ssd1306_send_start(uint8_t type);
    void ssd1306_send_stop(void);
    void ssd1306_setpos(uint8_t x, uint8_t y);
    void ssd1306_fillscreen(uint8_t fill);
    void ssd1306_char_font6x8(char ch);
    void ssd1306_string_font6x8(char *s);
    void ssd1306_draw_bmp(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, const uint8_t bitmap[]);


    void     write8 (uint16_t framAddr, uint8_t value);
    uint8_t  read8  (uint16_t framAddr);

  private:
    uint8_t i2c_addr;
    boolean _framInitialised;
};

// ----------------------------------------------------------------------------

#endif
