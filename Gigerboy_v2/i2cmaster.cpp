#include <stdlib.h>
#include <avr/io.h>
#include <math.h>

#define SDA_PORT PORTB
#define SDA_PIN 0 // = A4
#define SCL_PORT PORTB
#define SCL_PIN 2 // = A5
#define I2C_FASTMODE 1
#include <SoftI2CMaster.h>

#include <avr/pgmspace.h>
#include "i2cmaster.h"
#include "font6x8tinym.h"


const uint8_t ssd1306_init_sequence [] PROGMEM = {	// Initialization Sequence
  0xAE,			// Display OFF (sleep mode)
  0x20, 0b00,		// Set Memory Addressing Mode
  // 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
  // 10=Page Addressing Mode (RESET); 11=Invalid
  0xB0,			// Set Page Start Address for Page Addressing Mode, 0-7
  0xC8,			// Set COM Output Scan Direction
  0x00,			// ---set low column address
  0x10,			// ---set high column address
  0x40,			// --set start line address
  0x81, 0x3F,		// Set contrast control register
  0xA1,			// Set Segment Re-map. A0=address mapped; A1=address 127 mapped.
  0xA6,			// Set display mode. A6=Normal; A7=Inverse
  0xA8, 0x3F,		// Set multiplex ratio(1 to 64)
  0xA4,			// Output RAM to Display
  // 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
  0xD3, 0x00,		// Set display offset. 00 = no offset
  0xD5,			// --set display clock divide ratio/oscillator frequency
  0xF0,			// --set divide ratio
  0xD9, 0x22,		// Set pre-charge period
  0xDA, 0x12,		// Set com pins hardware configuration
  0xDB,			// --set vcomh
  0x20,			// 0x20,0.77xVcc
  0x8D, 0x14,		// Set DC-DC enable
  0xAF			// Display ON in normal mode

};

/*========================================================================*/
/*                            CONSTRUCTORS                                */
/*========================================================================*/

/**************************************************************************/
/*!
    Constructor
*/
/**************************************************************************/
i2cmaster::i2cmaster(void)
{
  _framInitialised = false;
}

void i2cmaster::begin(uint8_t addr)
{
  i2c_init();
  i2c_addr = addr;
  _delay_ms(40);
  for (uint8_t i = 0; i < sizeof (ssd1306_init_sequence); i++) {
    ssd1306_send_command(pgm_read_byte(&ssd1306_init_sequence[i]));
  }
  _framInitialised = true;
}

void i2cmaster::ssd1306_send_byte(uint8_t type, uint8_t byte) {
  if (i2c_write(byte) == 0) {
    ssd1306_send_stop();
    ssd1306_send_start(type);
    i2c_write(byte);
  }
}

void i2cmaster::ssd1306_send_start(uint8_t type) {
  i2c_start(SSD1306_SA << 1 | I2C_WRITE);
  i2c_write(type);
}

void i2cmaster::ssd1306_send_stop(void) {
  i2c_stop();
}

void i2cmaster::ssd1306_send_command(uint8_t command)
{
  ssd1306_send_start(0x00);
  ssd1306_send_byte(0x00, command);
  ssd1306_send_stop();
}

void i2cmaster::ssd1306_setpos(uint8_t x, uint8_t y)
{
  ssd1306_send_start(0x00);
  ssd1306_send_byte(0x00, 0xb0 + y);
  ssd1306_send_byte(0x00, ((x & 0xf0) >> 4) | 0x10); // | 0x10
  ssd1306_send_byte(0x00, (x & 0x0f) | 0x01); // | 0x01
  ssd1306_send_stop();
}

void i2cmaster::ssd1306_fillscreen(uint8_t fill)
{
  uint8_t m, n;
  for (m = 0; m < 8; m++)
  {
    ssd1306_send_command(0xb0 + m);	// page0 - page1
    ssd1306_send_command(0x00);		// low column start address
    ssd1306_send_command(0x10);		// high column start address
    ssd1306_send_start(0x40);
    for (n = 0; n < 128; n++)
    {
      ssd1306_send_byte(0x40, fill);
    }
    ssd1306_send_stop();
  }
}

void i2cmaster::ssd1306_char_font6x8(char ch) {
  uint8_t i;
  uint8_t c = ch - 32;
  ssd1306_send_start(0x40);
  for (i = 0; i < 6; i++)
  {
    ssd1306_send_byte(0x40, pgm_read_byte(&ssd1306xled_font6x8[c * 6 + i]));
  }
  ssd1306_send_stop();
}

void i2cmaster::ssd1306_string_font6x8(char *s) {
  while (*s) {
    ssd1306_char_font6x8(*s++);
  }
}

void i2cmaster::ssd1306_draw_bmp(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, const uint8_t bitmap[])
{
  uint16_t j = 0;
  uint8_t y, x;
  if (y1 % 8 == 0) y = y1 / 8;
  else y = y1 / 8 + 1;
  for (y = y0; y < y1; y++)
  {
    ssd1306_setpos(x0, y);
    ssd1306_send_start(0x40);
    for (x = x0; x < x1; x++)
    {
      ssd1306_send_byte(0x40, pgm_read_byte(&bitmap[j++]));
    }
    ssd1306_send_stop();
  }
}


// ----------------------------------------------------------------------------



/*========================================================================*/
/*                           PUBLIC FUNCTIONS                             */
/*========================================================================*/

/**************************************************************************/
/*!
    Initializes I2C and configures the chip (call this function before
    doing anything else)
*/
/**************************************************************************/


/**************************************************************************/
/*!
    @brief  Writes a byte at the specific FRAM address

    @params[in] i2cAddr
                The I2C address of the FRAM memory chip (1010+A2+A1+A0)
    @params[in] framAddr
                The 16-bit address to write to in FRAM memory
    @params[in] i2cAddr
                The 8-bit value to write at framAddr
*/
/**************************************************************************/
void i2cmaster::write8 (uint16_t framAddr, uint8_t value)
{
  i2c_start((i2c_addr << 1) | I2C_WRITE);
  i2c_write(framAddr >> 8);
  i2c_write(framAddr & 0xFF);
  i2c_write(value);
  i2c_stop();
}

/**************************************************************************/
/*!
    @brief  Reads an 8 bit value from the specified FRAM address

    @params[in] i2cAddr
                The I2C address of the FRAM memory chip (1010+A2+A1+A0)
    @params[in] framAddr
                The 16-bit address to read from in FRAM memory

    @returns    The 8-bit value retrieved at framAddr
*/
/**************************************************************************/
uint8_t i2cmaster::read8 (uint16_t framAddr)
{
  i2c_start((i2c_addr << 1) | I2C_WRITE);
  i2c_write(framAddr >> 8);
  i2c_write(framAddr & 0xFF);
  i2c_stop();

  i2c_start((i2c_addr << 1) | I2C_READ);

  uint8_t result = i2c_read(true);
  i2c_stop();
  return result;
}
